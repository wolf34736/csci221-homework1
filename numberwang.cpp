#include <iostream>
#include <stdlib.h>
using namespace std;

bool numberwang(int x);

int number_wang = rand() % 10 + 1;

int main(){
	string p1, p2;

	cout << "What is player 1's name?" << endl;
	cin >> p1;
   
	cout << "What is player 2's name?" << endl;
	cin >> p2;

	int rounds;
	
	cout << "How many rounds?" << endl;
	cin >> rounds;

	int first = 1;
	int turn = 1;
	int current_round = 1;
	int p1_score = 0;
	int p2_score = 0;
	
	while(rounds >= current_round){
		if(first == 1){
			cout << "Round " << current_round << " " <<  p1 << " to play first." << endl;
			first = 2;
			turn = 1;
			p1_score += 9;
			p2_score += 3;
		}
		else{
			cout << "Round " << current_round << " " <<  p2 << " to play first." << endl;
			first = 1;
			turn = 2;
			p1_score += 3;
			p2_score += 9;
		}
		bool over = true;
		while(over){
			int guess;

			if(turn == 1){
				cout << p1 << ":" << endl;
				cin >> guess;
				turn = 2;
			}
			else{
				cout << p2 << ":" << endl;
				cin >> guess;
				turn = 1;
			}
			if(numberwang(guess)){
				if(turn == 1){
					p2_score += rand() % 13 + 1;
					p1_score += rand() % 9;
				}
				else{
					p1_score += rand() % 13 + 12;
					p2_score += rand() % rand() % 5 + 1;
				}
				over = false;
			}
		}
		current_round++;
	}
	if(p2_score >= p1_score){
		p2_score += 5;
		cout << "Final scores: " << p2 << " pulls ahead with " << p2_score << ", and " <<  p1 << " finishes with "<< p1_score << "." << endl;
	}
	else{
		p1_score += 13;
		cout << "Final scores: " << p1 << " pulls ahead with " << p1_score << ", and " <<  p2 << " finishes with "<< p2_score << "." << endl;

	}
		

}

bool numberwang(int x){
	if(number_wang == x){
		cout << "That's Numberwang!" << endl;
		number_wang = rand() % 10 + 1;
		return true;
	}
	else{
		return false;
	}
}
